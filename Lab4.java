import java.util.ArrayList;
	import java.util.Arrays;
	import java.util.List;
	import java.util.Map;
	import java.util.TreeMap;
public class Lab4 {
	
		/**
		 * Run the exercises to ensure we got the right answers
		 */
		public void runExercises() {
			System.out.println("Running exercise 1 solution...");
			exercise1();
			System.out.println("Running exercise 2 solution...");
			exercise2();
			System.out.println("Running exercise 3 solution...");
			exercise3();
			System.out.println("Running exercise 4 solution...");
			exercise4();
			
		}

		/**
		 * All exercises should be completed using Lambda expressions and the new
		 * methods added to JDK 8 where appropriate. There is no need to use an
		 * explicit loop in any of the code. Use method references rather than full
		 * lambda expressions wherever possible.
		 */
		/**
		 * Exercise 1
		 *
		 * Create a string that consists of the first letter of each word in the
		 * list of Strings provided.
		 */
		private void exercise1() {
			List<String> list = Arrays.asList("alpha", "bravo", "charlie", "delta", "echo", "foxtrot");

			list.forEach(i -> System.out.println(i.charAt(0)));
			
		}

		/**
		 * Exercise 2
		 *
		 * Remove the words that have even lengths from the list.
		 */
		private void exercise2() {
			List<String> list = new ArrayList<>(Arrays.asList("alpha", "bravo", "charlie", "delta", "echo", "foxtrot"));

			list.forEach(i -> {if (i.length() % 2 == 0) System.out.println(i);});
		}

		/**
		 * Exercise 3
		 *
		 * Replace every word in the list with its upper case equivalent.
		 */
		private void exercise3() {
			List<String> list = new ArrayList<>(Arrays.asList("alpha", "bravo", "charlie", "delta", "echo", "foxtrot"));

			list.forEach(i -> System.out.println(i.toUpperCase()));
		}

		/**
		 * Exercise 4
		 *
		 * Convert every key-value pair of the map into a string and append them all
		 * into a single string, in iteration order.
		 */
		private void exercise4() {
			
			Map<String, Integer> map = new TreeMap<>();
			map.put("c", 3);
			map.put("b", 2);
			map.put("a", 1);

			map.forEach((k, i) -> i.toString());
			map.forEach((k, i) -> System.out.println(k + ": " + i));
		}

		

		/**
		 * Main entry point for application
		 *
		 * @param args
		 *            the command line arguments
		 */
		public static void main(String[] args) {
			Lab4 lab = new Lab4();
			lab.runExercises();
		}
	}

